package linkparser

// Link contains the hyperlink reference and the text
type Link struct {
	Href string
	Text string
}
