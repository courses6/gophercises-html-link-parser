package linkparser

import (
	"errors"
	"io"
	"strings"

	"golang.org/x/net/html"
)

var (
	// NoLinkFoundMsg ...
	NoLinkFoundMsg = "No links haven been found."
	// ParseErrorMsg ...
	ParseErrorMsg = "Error when parsing file."
)

// ExtractLinksUseCase ...
type ExtractLinksUseCase struct {
	Parser LinkParser
}

// LinkParser ...
type LinkParser interface {
	Parse(io.Reader) (*html.Node, error)
}

// NewExtractLinksUseCase ...
func NewExtractLinksUseCase(parser LinkParser) *ExtractLinksUseCase {
	return &ExtractLinksUseCase{
		Parser: parser,
	}
}

// Execute ...
func (usecase *ExtractLinksUseCase) Execute(reader io.Reader) ([]Link, error) {
	doc, err := usecase.Parser.Parse(reader)
	if err != nil {
		return nil, errors.New(ParseErrorMsg)
	}

	nodes := findHyperLinkNodes(doc)
	var links []Link
	for _, link := range nodes {
		links = append(links, buildLink(link))
	}
	return links, err
}

func findHyperLinkNodes(node *html.Node) []*html.Node {
	if node.Type == html.ElementNode && node.Data == "a" && node.PrevSibling != nil && node.PrevSibling.Data == "a" {
		return nil
	}
	if node.Type == html.ElementNode && node.Data == "a" {
		return []*html.Node{node}
	}
	var nodes []*html.Node
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		nodes = append(nodes, findHyperLinkNodes(c)...)
	}
	return nodes
}

func buildLink(node *html.Node) Link {
	var link Link
	for _, attr := range node.Attr {
		switch attr.Key {
		case "href":
			link.Href = attr.Val
			break
		}
	}

	link.Text = extractTextFromElement(node)
	return link
}

func extractTextFromElement(node *html.Node) string {
	if node.Type == html.TextNode {
		return node.Data
	}
	if node.Type != html.ElementNode {
		return ""
	}
	var text string
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		text += extractTextFromElement(c)
	}
	return strings.Join(strings.Fields(text), " ")
}
