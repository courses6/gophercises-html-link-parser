package linkparser

import (
	"errors"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/net/html"
)

type MockParser struct {
	// Call func() ([]Link, error)
	Call func() (*html.Node, error)
}

func (mock *MockParser) Parse(io.Reader) (*html.Node, error) {
	return mock.Call()
}

var (
	usecase    ExtractLinksUseCase
	mockParser *MockParser
)

func TestExtractLinksUseCaseNoLink(t *testing.T) {
	// initialization
	mockParser = &MockParser{}
	mockParser.Call = func() (*html.Node, error) {
		return nil, errors.New(ParseErrorMsg)
	}
	usecase := NewExtractLinksUseCase(mockParser)
	file, openErr := os.Open("../../fixtures/example0.html")
	fmt.Println(openErr)

	// execution
	links, err := usecase.Execute(file)

	// assertion
	assert.NotNil(t, err)
	assert.EqualValues(t, ParseErrorMsg, err.Error())
	assert.Nil(t, links)
}

func TestExtractLinksUseCaseSimpleLink(t *testing.T) {
	// initialization
	mockParser = &MockParser{}
	file, _ := os.Open("../../fixtures/example1.html")
	mockParser.Call = func() (*html.Node, error) {
		node, _ := html.Parse(file)
		return node, nil
	}
	usecase := NewExtractLinksUseCase(mockParser)

	// execution
	links, err := usecase.Execute(file)

	// assertion
	assert.Nil(t, err)
	assert.NotNil(t, links)
	assert.Len(t, links, 1)
	assert.EqualValues(t, "/other-page", links[0].Href)
	assert.EqualValues(t, "A link to another page", links[0].Text)

}

func TestExtractLinksUseCaseNestedLinks(t *testing.T) {
	// initialization
	mockParser = &MockParser{}
	file, _ := os.Open("../../fixtures/example5.html")
	mockParser.Call = func() (*html.Node, error) {
		node, _ := html.Parse(file)
		return node, nil
	}
	usecase := NewExtractLinksUseCase(mockParser)

	// execution
	links, err := usecase.Execute(file)

	// assertion
	assert.Nil(t, err)
	assert.NotNil(t, links)
	assert.Len(t, links, 1)
	assert.EqualValues(t, "/other-page", links[0].Href)
	assert.EqualValues(t, "A link to another page", links[0].Text)
}

func TestExtractLinksUseCaseMultipleLinksWithElement(t *testing.T) {
	// initialization
	mockParser = &MockParser{}
	file, _ := os.Open("../../fixtures/example2.html")
	mockParser.Call = func() (*html.Node, error) {
		node, _ := html.Parse(file)
		return node, nil
	}
	usecase := NewExtractLinksUseCase(mockParser)

	// execution
	links, err := usecase.Execute(file)

	// assertion
	assert.Nil(t, err)
	assert.NotNil(t, links)
	assert.Len(t, links, 2)
	assert.EqualValues(t, "https://www.twitter.com/joncalhoun", links[0].Href)
	assert.EqualValues(t, "Check me out on twitter", links[0].Text)
	assert.EqualValues(t, "https://github.com/gophercises", links[1].Href)
	assert.EqualValues(t, "Gophercises is on Github", links[1].Text)
}

func TestExtractLinksUseCaseLinkWithHtmlElement(t *testing.T) {
	// see the example3 an <a> tag with <i> (icon)
	assert.Fail(t, "Failed")
}
