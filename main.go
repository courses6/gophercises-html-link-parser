package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/courses6/gophercises-html-link-parser/domain/linkparser"
	"golang.org/x/net/html"
)

var (
	defaultURL = "https://www.github.com"
)

// HTMLParser ...
type HTMLParser struct{}

// Parse ...
func (parser *HTMLParser) Parse(reader io.Reader) (*html.Node, error) {
	return html.Parse(reader)
}

func main() {
	url := flag.String("url", defaultURL, "the url the links are extracted from")
	flag.Parse()

	fmt.Printf("Trying to get links from: %s\n", *url)
	resp, err := http.Get(*url)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	parser := &HTMLParser{}
	usecase := linkparser.NewExtractLinksUseCase(parser)
	reader := bytes.NewReader(body)
	links, err := usecase.Execute(reader)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v", links)
}
