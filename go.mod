module gitlab.com/courses6/gophercises-html-link-parser

go 1.14

require (
	github.com/stretchr/testify v1.5.1
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120
)
